﻿using AutoMapper;
using System;
using System.Linq;
using System.Threading.Tasks;
using TBC.Domain.DataModels;
using TBC.Domain.ViewModels;
using TBC.Domain.ViewModels.HelperViewModels;
using TBC.Interfaces.DAL;
using TBC.Interfaces.Services;

namespace TBC.Services.StudentsServices
{
    public class StudentService : IStudentService
    {
        private readonly IStudentReader _studentReader;
        private readonly IStudentWriter _studentWriter;
        private readonly IMapper _mapper;

        public StudentService(IStudentReader studentReader, IStudentWriter studentWriter, IMapper mapper)
        {
            _studentReader = studentReader;
            _studentWriter = studentWriter;
            _mapper = mapper;
        }

        public async Task<ResultVievModel<StudentsViewModel>> GetSudentByIdAsync(Guid id)
        {
            var student = await _studentReader.GetSudentByIdAsync(id);
            var studentViewModel = _mapper.Map<Students, StudentsViewModel>(student);

            return new ResultVievModel<StudentsViewModel>(studentViewModel);
        }

        public async Task<ArrayResultViewModel<StudentsViewModel>> GetAllStudentsAsync(int skip, int take,
            string number, DateTimeOffset birthdayFrom, DateTimeOffset birthdayTo)
        {
            var students = (await _studentReader.GetAllStudentsAsync(number, birthdayFrom, birthdayTo)).ToList();
            var studentsViewModels = students.Skip(skip).Take(take).Select(student => _mapper.Map<Students, StudentsViewModel>(student));

            return new ArrayResultViewModel<StudentsViewModel>(studentsViewModels, skip, take, students.Count);
        }

        public async Task<ResultVievModel<StudentsViewModel>> InsertStudentAsync(StudentsViewModel student)
        {
            var duplicatedStudent = await _studentReader.GetSudentByNumberAsync(student.Number);
            if (duplicatedStudent != null )
            {
                return new ResultVievModel<StudentsViewModel>(new ErrorViewModel("Student with this number exists",
                    -1));
            }

            var studentResult =
                await _studentWriter.InsertStudentAsync(_mapper.Map<StudentsViewModel, Students>(student));
            var studentResultViewModel = _mapper.Map<Students, StudentsViewModel>(studentResult);

            return new ResultVievModel<StudentsViewModel>(studentResultViewModel);
        }

        public async Task<ResultVievModel<StudentsViewModel>> UpdateStudentByIdAsync(Guid id, StudentsViewModel newStudent)
        {

            var duplicatedStudent = await _studentReader.GetSudentByNumberAsync(newStudent.Number);
            if (duplicatedStudent != null && !duplicatedStudent.Id.Equals(id))
            {
                return new ResultVievModel<StudentsViewModel>(new ErrorViewModel("Student with this number exists",
                    -1));
            }

            var studentResult = await _studentWriter.UpdateStudentByIdAsync(id, _mapper.Map<StudentsViewModel, Students>(newStudent));
            var studentResultViewModel = _mapper.Map<Students, StudentsViewModel>(studentResult);

            return new ResultVievModel<StudentsViewModel>(studentResultViewModel);
        }

        public async Task<ResultVievModel<StudentsViewModel>> DeleteStudentByIdAsync(Guid id)
        {
            var result = await _studentWriter.DeleteStudentByIdAsync(id);

            return result
                ? new ResultVievModel<StudentsViewModel>()
                : new ResultVievModel<StudentsViewModel>(new ErrorViewModel("Can not delete this student", -1));
        }
    }
}
