﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TBC.Domain.DataModels;
using TBC.Interfaces.DAL;

namespace TBC.DAL.StudentsRepositories
{
    public class StudentRepository : IStudentReader, IStudentWriter
    {
        private readonly TBCContext _context;
        private readonly IMapper _mapper;

        public StudentRepository(TBCContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public async Task<Students> GetSudentByIdAsync(Guid id)
        {
            return await _context.Students.SingleOrDefaultAsync(x => x.Id.Equals(id));
        }

        public async Task<IEnumerable<Students>> GetAllStudentsAsync(string number, DateTimeOffset birthdayFrom,
            DateTimeOffset birthdayTo)
        {
            return await _context.Students
                .Where(x => x.Number.Contains(number) && x.Birthday >= birthdayFrom && x.Birthday <= birthdayTo)
                .ToListAsync();
        }

        public async Task<Students> GetSudentByNumberAsync(string number)
        {
            return await _context.Students.SingleOrDefaultAsync(x => x.Number.Equals(number));
        }

        public async Task<Students> InsertStudentAsync(Students student)
        {
            var newStudent = await _context.Students.AddAsync(student);
            await _context.SaveChangesAsync();
            return newStudent.Entity;
        }

        public async Task<Students> UpdateStudentByIdAsync(Guid id, Students newStudent)
        {
            var student = await _context.Students.SingleOrDefaultAsync(x => x.Id.Equals(id));

            if (student != null)
            {
                _mapper.Map(newStudent, student);
                _context.Students.Update(student);
                await _context.SaveChangesAsync();
            }

            return student;
        }

        public async Task<bool> DeleteStudentByIdAsync(Guid id)
        {
            var studentForDelete = await _context.Students.Where(x => x.Id.Equals(id)).ToListAsync();
            _context.Students.RemoveRange(studentForDelete);
            var result = await _context.SaveChangesAsync();

            return result != 0;
        }
    }
}
