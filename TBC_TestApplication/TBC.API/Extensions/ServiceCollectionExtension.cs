﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using TBC.DAL.StudentsRepositories;
using TBC.Domain.AutoMapperProfilers;
using TBC.Interfaces.DAL;
using TBC.Interfaces.Services;
using TBC.Services.StudentsServices;

namespace TBC.API.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection CreateDependencies(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IStudentReader, StudentRepository>();
            serviceCollection.AddTransient<IStudentWriter, StudentRepository>();

            serviceCollection.AddTransient<IStudentService, StudentService>();


            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new StudentProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            serviceCollection.AddSingleton(mapper);

            return serviceCollection;
        }
    }
}
