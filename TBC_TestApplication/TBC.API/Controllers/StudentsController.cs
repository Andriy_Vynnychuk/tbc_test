﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TBC.Domain.ViewModels;
using TBC.Interfaces.Services;

namespace TBC.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly IStudentService _studentService;

        public StudentsController(IStudentService studentService)
        {
            _studentService = studentService;
        }

        // GET: api/Students
        [HttpGet]
        public async Task<IActionResult> Get(int skip = 0, int take = 10, string number = null,
            DateTimeOffset? birthdayFrom = null, DateTimeOffset? birthdayTo = null)
        {
            var result = await _studentService.GetAllStudentsAsync(skip, take, number ?? string.Empty,
                birthdayFrom ?? DateTimeOffset.MinValue, birthdayTo ?? DateTimeOffset.MaxValue);

            return Ok(result);
        }

        // GET: api/Students/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(Guid id)
        {
            var result = await _studentService.GetSudentByIdAsync(id);

            return Ok(result);
        }

        // POST: api/Students
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] StudentsViewModel student)
        {
            var result = await _studentService.InsertStudentAsync(student);

            return Ok(result);
        }

        // PUT: api/Students/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] StudentsViewModel student)
        {
            var result = await _studentService.UpdateStudentByIdAsync(id, student);

            return Ok(result);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _studentService.DeleteStudentByIdAsync(id);

            return Ok(result);
        }
    }
}
