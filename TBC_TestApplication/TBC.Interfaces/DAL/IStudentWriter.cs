﻿using System;
using System.Threading.Tasks;
using TBC.Domain.DataModels;

namespace TBC.Interfaces.DAL
{
    public interface IStudentWriter
    {
        Task<Students> InsertStudentAsync(Students student);
        Task<Students> UpdateStudentByIdAsync(Guid id, Students newStudent);
        Task<bool> DeleteStudentByIdAsync(Guid id);
    }
}