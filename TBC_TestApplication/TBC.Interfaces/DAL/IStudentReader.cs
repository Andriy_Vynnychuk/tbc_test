﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TBC.Domain.DataModels;

namespace TBC.Interfaces.DAL
{
    public interface IStudentReader
    {
        Task<Students> GetSudentByIdAsync(Guid id);
        Task<IEnumerable<Students>> GetAllStudentsAsync( string number, DateTimeOffset birthdayFrom,
            DateTimeOffset birthdayTo);
        Task<Students> GetSudentByNumberAsync(string number);
    }
}