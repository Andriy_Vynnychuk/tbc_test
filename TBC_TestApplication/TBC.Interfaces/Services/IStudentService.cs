﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TBC.Domain.ViewModels;
using TBC.Domain.ViewModels.HelperViewModels;

namespace TBC.Interfaces.Services
{
    public interface IStudentService
    {
        Task<ResultVievModel<StudentsViewModel>> GetSudentByIdAsync(Guid id);
        Task<ArrayResultViewModel<StudentsViewModel>> GetAllStudentsAsync(int skip, int take, string number, DateTimeOffset birthdayFrom, DateTimeOffset birthdayTo);
        Task<ResultVievModel<StudentsViewModel>> InsertStudentAsync(StudentsViewModel student);
        Task<ResultVievModel<StudentsViewModel>> UpdateStudentByIdAsync(Guid id, StudentsViewModel newStudent);
        Task<ResultVievModel<StudentsViewModel>> DeleteStudentByIdAsync(Guid id);
    }
}