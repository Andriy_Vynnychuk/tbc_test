﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TBC.Domain.ViewModels.HelperViewModels
{
    public class ArrayResultViewModel<T>
    {
        public bool IsSuccess { get; }
        public IEnumerable<T> Data { get; }
        public int StartIndex { get; }
        public int Take { get; }
        public int Total { get; }
        public ErrorViewModel Error { get; }

        public ArrayResultViewModel(IEnumerable<T> data, int startIndex, int take, int total)
        {
            IsSuccess = true;
            Data = data;
            StartIndex = startIndex;
            Take = take;
            Total = total;
        }

        public ArrayResultViewModel(IEnumerable<T> data)
        {
            var enumerable = data.ToList();
            IsSuccess = true;
            Data = enumerable;
            StartIndex = 0;
            Take = Int32.MaxValue;
            Total = enumerable.Count;
        }

        public ArrayResultViewModel(ErrorViewModel error)
        {
            IsSuccess = false;
            Error = error;
        }
    }
}
