﻿namespace TBC.Domain.ViewModels.HelperViewModels
{
    public class ResultVievModel<T>
    {
        public bool IsSuccess { get; }
        public T Data { get; }
        public ErrorViewModel Error { get; }

        public ResultVievModel()
        {
            IsSuccess = true;
        }

        public ResultVievModel(T data)
        {
            IsSuccess = true;
            Data = data;
        }

        public ResultVievModel(ErrorViewModel error)
        {
            IsSuccess = false;
            Error = error;
        }
    }
}
