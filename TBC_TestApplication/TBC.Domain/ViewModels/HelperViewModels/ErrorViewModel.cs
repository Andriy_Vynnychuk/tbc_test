﻿namespace TBC.Domain.ViewModels.HelperViewModels
{
    public class ErrorViewModel
    {
        public string Message { get; }
        public int ErrorCode { get; }

        public ErrorViewModel(string message, int errorCode1)
        {
            Message = message;
            ErrorCode = errorCode1;
        }
    }
}
