﻿using System;
using TBC.Domain.Enums;

namespace TBC.Domain.DataModels
{
    public class Students
    {
        public Guid Id { get; set; }
        public string Number { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTimeOffset Birthday { get; set; }
        public Gender Gender { get; set; }
    }
}
