﻿using Microsoft.EntityFrameworkCore;

namespace TBC.Domain.DataModels
{
    public sealed class TBCContext : DbContext
    {
        private static bool _created;
        public TBCContext()
        {
            if (!_created)
            {
                _created = true;
                Database.EnsureDeleted();
                Database.EnsureCreated();
            }
        }

        public TBCContext (DbContextOptions<TBCContext> options)
            : base(options)
        {
            if (!_created)
            {
                _created = true;
                Database.EnsureDeleted();
                Database.EnsureCreated();
            }
        }

        public DbSet<Students> Students { get; set; }
    }
}
