﻿using AutoMapper;
using TBC.Domain.DataModels;
using TBC.Domain.ViewModels;

namespace TBC.Domain.AutoMapperProfilers
{
    public class StudentProfile : Profile
    {
        public StudentProfile()
        {
            CreateMap<Students, StudentsViewModel>().ReverseMap();
            CreateMap<Students, Students>().ForMember(x => x.Id, opt => opt.Ignore());
        }
    }
}
