﻿namespace TBC.Domain.Enums
{
    public enum Gender
    {
        Undefined,
        Male,
        Female
    }
}