﻿using System;
using FluentValidation;
using TBC.Domain.ViewModels;

namespace TBC.Domain.Validators
{
    public class StudentValidator : AbstractValidator<StudentsViewModel>
    {
        public StudentValidator()
        {
            RuleFor(x => x.Birthday).NotNull().NotEmpty().LessThanOrEqualTo(DateTimeOffset.Now.AddYears(-16));
            RuleFor(x => x.FirstName).NotNull().NotEmpty();
            RuleFor(x => x.Gender).NotNull().NotEmpty();
            RuleFor(x => x.LastName).NotNull().NotEmpty();
            RuleFor(x => x.Number).NotNull().NotEmpty();
        }
    }
}
