import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StudentsService } from '../students.service';
import StudentViewModel from '../models/studentViewModel';

@Component({
  selector: 'app-student-edit',
  templateUrl: './student-edit.component.html',
  styleUrls: ['./student-edit.component.css']
})
export class StudentEditComponent implements OnInit {

  angForm: FormGroup;
  student: any = {};
  genders: any = [
    {"value": 0, "text": "Undefined"}, 
    {"value": 1, "text": "Male"}, 
    {"value": 2, "text": "Female"}
  ]

  constructor(private route: ActivatedRoute, private router: Router, private ss: StudentsService, private fb: FormBuilder) {
      this.createForm();
 }

  createForm() {
    this.angForm = this.fb.group({
      number: ['', Validators.required ],
      firstName: ['', Validators.required ],
      lastName: ['', Validators.required ],
      birthday: ['', [Validators.required, this.olderThan] ],
      gender: ['', [Validators.required, Validators.min(1)] ],
    });
    this.ss.form = this.angForm;
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
        this.ss.editStudent(params['id']).subscribe((res: StudentViewModel) => {
          this.student = res.data;
      });
    });
  }
  
  updateStudent(number, firstName, lastName, birthday, gender, id) {
    this.route.params.subscribe(params => {
      this.ss.updateStudent(number, firstName, lastName, birthday, gender, params.id);
    });
  }  

  olderThan(control:AbstractControl):{ [key: string]: any; } {
    if (new Date(control.value).getTime() > new Date().setFullYear(new Date().getFullYear() - 16)) {
      return {olderThan: true};
    } else {
      return null;
    }
  }
}