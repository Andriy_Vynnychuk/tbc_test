import { Component, Input, OnInit } from '@angular/core';
import Student from '../models/Student';
import { Gender } from '../models/gender';
import StudentArrayViewModel from '../models/studentArrayViewModel';
import { StudentsService } from '../students.service';
import { ActivatedRoute } from '@angular/router';
import StudentViewModel from '../models/studentViewModel';
import { filter } from 'minimatch';

@Component({
  selector: 'app-student-get',
  templateUrl: './student-get.component.html',
  styleUrls: ['./student-get.component.css']
})
export class StudentGetComponent implements OnInit {
  Gender = Gender;
  pager = {};
  students: Student[] = [];
  take = 10;
  @Input() groupFilters: any;

  constructor(
    private ss: StudentsService,
    private route: ActivatedRoute
    ) { }

    ngOnInit() {
    this.route.queryParams.subscribe(x => {
      let filter = this.groupFilters || {}
      this.ss
      .getStudents((x.page - 1 || 0) * this.take, this.take, filter.number, filter.birthdayFrom, filter.ageto)
      .subscribe((data: StudentArrayViewModel) => {
        this.updateStudentList(data)
      })
    });
  }
  
  ngOnChanges() {
    if (this.groupFilters) this.filterUserList(this.groupFilters);
  }

  filterUserList(filters: any): void {
		this.route.queryParams.subscribe(x => {
      this.ss
      .getStudents((x.page - 1 || 0) * this.take, this.take, filters.number, filters.birthdayFrom, filters.ageto)
      .subscribe((data: StudentArrayViewModel) => {
        this.updateStudentList(data);
      })
    });
  }

  updateStudentList(data: StudentArrayViewModel) {
        this.students = data.data;
        this.pager = {
          currentPage: (data.startIndex / data.take) + 1,
          pages: [...Array(Math.ceil(data.total / data.take)).keys()].map(x => ++x),
          totalPages: Math.ceil(data.total / data.take)
        };
  }

  deleteStudent(id) {
    this.ss.deleteStudent(id).subscribe((data: StudentViewModel) => {
      if (data.isSuccess) {
        this.groupFilters 
            ? this.filterUserList(this.groupFilters) 
            : this.filterUserList({});
      }
    });
}

}