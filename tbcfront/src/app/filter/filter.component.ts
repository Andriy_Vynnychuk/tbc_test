import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { StudentsService } from '../students.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  @Output() groupFilters: EventEmitter<any> = new EventEmitter<any>();
  form: FormGroup;
	searchText: string = '';
	constructor(private fb: FormBuilder,
	private ss: StudentsService) {}
	ngOnInit(): void {
		this.buildForm();
	}
	buildForm(): void {
		this.form = this.fb.group({
			number: new FormControl(''),
			birthdayFrom: new FormControl(''),
			ageto: new FormControl('')
		});
	}

  search(filters: any): void {
    Object.keys(filters).forEach(key => filters[key] === '' ? delete filters[key] : key);
    this.groupFilters.emit(filters);
  }
}
