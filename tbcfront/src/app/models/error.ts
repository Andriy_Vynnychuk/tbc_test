export default class Error {
    message: string;
    errorCode: number;
}