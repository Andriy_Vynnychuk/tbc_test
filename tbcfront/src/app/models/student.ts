import { Gender } from './gender'
export default class Student {
    id: string;
    number: string;
    firstName: number;
    lastName: number;
    birthday: Date;
    gender: Gender;
  }