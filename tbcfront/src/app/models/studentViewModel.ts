import Student from "./student"
import Error from "./error"

export default class StudentViewModel {
    isSuccess: boolean;
    data: Student;
    error: Error;
}