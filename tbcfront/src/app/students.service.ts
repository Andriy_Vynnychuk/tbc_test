import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import StudentViewModel from './models/studentArrayViewModel';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  uri = 'https://localhost:44358/api/Students';
  form = null;

  constructor(
    private http: HttpClient, 
    private router: Router
    ) { }

  addStudent(number, firstName, lastName, birthday, gender) {
    const obj = {
      number, 
      firstName, 
      lastName, 
      birthday, 
      gender
    };
    console.log(obj);
    this.http.post(`${this.uri}`, obj)
    .subscribe((res: StudentViewModel) => {
      if (res.isSuccess) {
        this.router.navigate(['students']);
      }
      else {
        if (res.error.errorCode === -1) {
          const formControl = this.form.get('number');
          if (formControl) {
            formControl.setErrors({
              duplicate: true
            });
          }
        }
      }
    });
  }
  
  getStudents(skip: number, take: number, number: string = null, birthdayFrom: Date = null, birthdayTo: Date = null) {
    let url = `${this.uri}?skip=${skip}&take=${take}`
    if (number !== null) {
      url += `&number=${number}`
    }
    if (birthdayFrom !== null) {
      url += `&birthdayFrom=${birthdayFrom}`
    }
    if (birthdayTo !== null) {
      url += `&birthdayTo=${birthdayTo}`
    }
    let result = this
           .http
           .get(url);
    return result;
  }

  editStudent(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
  }

  async updateStudent(number, firstName, lastName, birthday, gender, id) {
    const obj = {
      number, 
      firstName, 
      lastName, 
      birthday, 
      gender
    };
    await this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe((res: StudentViewModel) => {
        if (res.isSuccess) {
          this.router.navigate(['students']);
        }
        else {
          if (res.error.errorCode === -1) {
            const formControl = this.form.get('number');
            if (formControl) {
              formControl.setErrors({
                duplicate: true
              });
            }
          }
        }
      });
  }

  deleteStudent(id) {
    return this
        .http
        .delete(`${this.uri}/${id}`);
  }
}