import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { StudentsService } from '../students.service';

@Component({
  selector: 'app-student-add',
  templateUrl: './student-add.component.html',
  styleUrls: ['./student-add.component.css']
})
export class StudentAddComponent implements OnInit {

  angForm: FormGroup;
  genders: any = [
    {"value": 0, "text": "Undefined"}, 
    {"value": 1, "text": "Male"}, 
    {"value": 2, "text": "Female"}
  ]
  constructor(private fb: FormBuilder, private ss: StudentsService) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      number: ['', Validators.required ],
      firstName: ['', Validators.required ],
      lastName: ['', Validators.required ],
      birthday: ['', [Validators.required, this.olderThan] ],
      gender: ['', [Validators.required, Validators.min(1)] ],
    });
    this.ss.form = this.angForm;
  }

  addStudent(number, firstName, lastName, birthday, gender) {
    this.ss.addStudent(number, firstName, lastName, birthday, gender);
    
  }

  ngOnInit() {
  }

  olderThan(control:AbstractControl):{ [key: string]: any; } {
    if (new Date(control.value).getTime() > new Date().setFullYear(new Date().getFullYear() - 16)) {
      return {olderThan: true};
    } else {
      return null;
    }
  }
}